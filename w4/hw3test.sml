(* Homework3 Simple Test*)
(* These are basic test cases. Passing these tests does not guarantee that your code will pass the actual homework grader *)
(* To run the test, add a new line to the top of this file: use "homeworkname.sml"; *)
(* All the tests should evaluate to true. For example, the REPL should say: val test1 = true : bool *)

val test1 = only_capitals ["A","B","C"] = ["A","B","C"]

val test2 = longest_string1 ["A","bc","C"] = "bc"
val test2b = longest_string1 ["A", "bc", "de", "f"] = "bc"
val test2c = longest_string1 [] = ""

val test3 = longest_string2 ["A","bc","C"] = "bc"
val test3b = longest_string2 ["A", "bc", "de", "f"] = "de"
val test3c = longest_string2 [] = ""
				      
val test4a = longest_string3 ["A","bc","C"] = "bc"
val test4a1 = longest_string3 ["A", "bc", "de", "f"] = "bc"
val test4a2 = longest_string3 [] = ""
						  
val test4b = longest_string4 ["A","B","C"] = "C"
val test4b1 = longest_string4 ["A", "bc", "de", "f"] = "de"
val test4b2 = longest_string4 [] = ""
						 
val test5 = longest_capitalized ["A","bc","C"] = "A"

val test6 = rev_string "abc" = "cba"

val test7 = first_answer (fn x => if x > 3 then SOME x else NONE) [1,2,3,4,5] = 4

val test8 = all_answers (fn x => if x = 1 then SOME [x] else NONE) [2,3,4,5,6,7] = NONE

val test9a = count_wildcards Wildcard = 1
val test9a1 = count_wildcards (ConstructorP("abc", Wildcard)) = 1
val test9a2 = count_wildcards (TupleP([Wildcard, UnitP, ConstP(7), ConstructorP("abc", Wildcard)])) = 2
val test9a3 = count_wildcards (TupleP([Wildcard, UnitP, Wildcard, Wildcard])) = 3
					    
val test9b = count_wild_and_variable_lengths (Variable("a")) = 1
val test9b1 = count_wild_and_variable_lengths (TupleP([Wildcard, Variable("bac"), UnitP, ConstP(7), ConstructorP("abc", Wildcard)])) = 5
													
val test9c = count_some_var ("x", Variable("x")) = 1
val test9c1 = count_some_var ("x", TupleP([Variable("xss"), Wildcard, Variable("x"), ConstructorP("x", Wildcard)])) = 1
															  
val test10 = check_pat (Variable("x")) = true

val test11 = match (Const(1), UnitP) = NONE

val test12 = first_match Unit [UnitP] = SOME []

val test110 = match (Tuple [Const 3, Unit, Constructor ("c0", Const 3), Constructor ("c1", Const 3)], TupleP [Variable "a", Wildcard, Variable "c0", ConstructorP ("c1", Variable "c1")]) = SOME [("c1",Const 3),("c0",Constructor ("c0",Const 3)),("a",Const 3)]

val tpl1 = Tuple [Const 3, Unit, Constructor ("c0", Const 3), Constructor ("c1", Const 3)]
val ptn1 = TupleP [Variable "a", Wildcard, Variable "c0", ConstructorP ("c1", Variable "c1")]
val ptn2 = TupleP [Variable "a", Wildcard, Variable "xx", ConstructorP ("c1", Variable "c1")]

