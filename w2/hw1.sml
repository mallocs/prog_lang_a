fun is_older(x : int*int*int, y : int*int*int) =
    if x = y
    then false
    else if (#1 x) < (#1 y)
    then true
    else if (#1 x) = (#1 y) andalso (#2 x) < (#2 y)
    then true
    else if (#1 x) = (#1 y) andalso (#2 x) = (#2 y) andalso (#3 x) < (#3 y)
    then true
    else false

	     
fun number_in_month(dates : (int*int*int) list, month : int) =
    if null dates
    then 0
    else if (#2 (hd dates)) = month
    then 1 + number_in_month(tl dates, month)
    else number_in_month(tl dates, month)

			
fun number_in_months(dates : (int*int*int) list, months : int list) =
    if null months
    then 0
    else number_in_month(dates, hd months) + number_in_months(dates, tl months)

							     
fun dates_in_month(dates : (int*int*int) list, month : int) =
    if null dates
    then []
    else if (#2 (hd dates)) = month
    then (hd dates)::dates_in_month((tl dates), month)
    else dates_in_month((tl dates), month)

		       
fun dates_in_months(dates : (int*int*int) list, months : int list) =
    if null months
    then []
    else dates_in_month(dates, hd months)@dates_in_months(dates, tl months)

							 
fun get_nth(strs : string list, position : int) =
    if position = 1
    then hd strs
    else get_nth(tl strs, position - 1)

		
fun date_to_string(date : (int*int*int)) =
    let val month_names = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    in
    get_nth(month_names, #2 date) ^ " " ^ Int.toString(#3 date) ^ ", " ^ Int.toString(#1 date)
    end

	
fun number_before_reaching_sum(sum : int, xs : int list) =
    if (hd xs) >= sum
    then 0
    else 1 + number_before_reaching_sum((sum - (hd xs)), tl xs)

				       
fun what_month(day_in_year : int) =
    let val month_lengths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    in
	1 + number_before_reaching_sum(day_in_year, month_lengths)
    end

	
fun month_range(day1 : int, day2 : int) =
    if day1 > day2
    then []
    else what_month(day1)::month_range(day1 + 1, day2)

				      
fun oldest(dates : (int*int*int) list) =
    if null dates
    then NONE
    else
	let val oldest_ans = oldest(tl dates) in
	    if isSome oldest_ans andalso is_older(valOf oldest_ans, hd dates)
	    then oldest_ans
	    else SOME (hd dates)
	end

	    
(* helper used by multiple challenge problems so leaving it in file scope *)
fun is_number_in_list(x : int, xs : int list) =
    if null xs
    then false
    else if x = hd xs
    then true
    else is_number_in_list(x, tl xs)

			  
fun number_in_months_challenge(dates : (int*int*int) list, months : int list) =
    if null months orelse null dates
    then 0
    else if is_number_in_list(#2 (hd dates), months)
    then 1 + number_in_months_challenge(tl dates, months)
    else number_in_months_challenge(tl dates, months)

				   
fun dates_in_months_challenge(dates : (int*int*int) list, months : int list) =
    if null months orelse null dates
    then []
    else if is_number_in_list(#2 (hd dates), months)
    then (hd dates)::dates_in_months_challenge(tl dates, months)
    else dates_in_months_challenge(tl dates, months)

				  
fun reasonable_date(date: (int*int*int)) =
    if #1 date <= 0
    then false
    else if #2 date <= 0 orelse #2 date > 12
    then false
    else if #3 date <= 0 orelse #3 date > 31
    then false
    else if #3 date = 31 andalso #2 date = 2 orelse #2 date = 4 orelse #2 date = 6 orelse #2 date = 9 orelse #2 date = 11
    then false
    else if #2 date = 2 andalso #3 date = 30 (* Feb. *)
    then false
    else if #2 date = 2 andalso #3 date = 29 andalso (#1 date) mod 4 <> 0 (* Feb. normal non-leap year *)
    then false
    else if #2 date = 2 andalso #3 date = 29 andalso (#1 date) mod 4 = 0 andalso (#1 date) mod 100 = 0 andalso (#1 date) mod 400 <> 0 (* Feb. exceptional non-leap years *)
    then false
    else true
