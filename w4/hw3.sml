(* Coursera Programming Languages, Homework 3, Provided Code *)

exception NoAnswer

datatype pattern = Wildcard
		 | Variable of string
		 | UnitP
		 | ConstP of int
		 | TupleP of pattern list
		 | ConstructorP of string * pattern

datatype valu = Const of int
	      | Unit
	      | Tuple of valu list
	      | Constructor of string * valu

fun g f1 f2 p =
    let 
	val r = g f1 f2 
    in
	case p of
	    Wildcard          => f1 ()
	  | Variable x        => f2 x
	  | TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
	  | ConstructorP(_,p) => r p
	  | _                 => 0
    end

(**** for the challenge problem only ****)

datatype typ = Anything
	     | UnitT
	     | IntT
	     | TupleT of typ list
	     | Datatype of string

(**** you can put all your code here ****)
fun only_capitals(xs : string list) =
    List.filter (fn x => Char.isUpper (String.sub (x, 0))) xs


fun longest_string1(xs : string list) =
    foldl (fn (s, acc) => if String.size s > String.size acc then s else acc) "" xs

	  
fun longest_string2(xs : string list) =
    foldl (fn (s, acc) => if String.size s >= String.size acc then s else acc) "" xs


fun longest_string_helper f = List.foldl (fn (s, acc) => if f (String.size s, String.size acc) then s else acc) ""

				    
val longest_string3 = longest_string_helper (fn (a, b) => a > b)

					    
val longest_string4 = longest_string_helper (fn (a, b) => a >= b)


val longest_capitalized = longest_string1 o only_capitals

						
val rev_string = implode o rev o explode


fun first_answer input_fn xs =
    case xs of
	[] => raise NoAnswer
      | x::xs' => case input_fn x of
		      NONE => first_answer input_fn xs'
		    | SOME i => i

				    
fun all_answers input_fn xs =
    let fun f (xs, acc) =
	    case xs of
		[] => SOME acc
	      | head::xs' => case input_fn head of
				 NONE => NONE
			      | SOME i => f(xs', acc @ i)
    in
	f(xs, [])
    end


val count_wildcards =
    let val count = 0
    in
	g (fn () => 1 + count) (fn x => count)
    end

	
val count_wild_and_variable_lengths =
    let val count = 0 in
	g (fn () => 1 + count) (fn x => count + String.size x)
    end


fun count_some_var (s, p) =
    let val count = 0 in
	g (fn () => count) (fn x => if x = s then 1 + count else count) p
    end


fun all_strings p =
    case p of
	Variable x => [x]
      | TupleP ps => List.foldl (fn (p,acc) => (all_strings p) @ acc) [] ps
      | ConstructorP(_,p) => all_strings p
      | _ => []
			
    
fun has_repeats xs =
    case xs of
	[] => false
      | head::xs' => List.exists (fn x => x = head orelse has_repeats xs') xs'

				 
val check_pat = not o has_repeats o all_strings;


fun match (v, p) =
    case (v,p) of
	(_, Wildcard) => SOME []
      | (_, Variable s) => SOME [(s, v)]
      | (_, UnitP) => if v = Unit then SOME [] else NONE
      | (_, ConstP n) => if v = Const n then SOME [] else NONE
      | (Tuple vs, TupleP ps) => let val result = all_answers match (ListPair.zip (vs,ps))
				 in
				     if length vs = length ps andalso result <> NONE then result else NONE
				 end
      | (Constructor(s2, v'), ConstructorP(s1,p')) => let val result = match(v', p')			
						      in
							  if s1 = s2 andalso result <> NONE
							  then result
							  else NONE
						      end
      | (_,_) => NONE 
 
		     
fun first_match v ps =
    SOME (first_answer match (List.map (fn p => (v,p)) ps))
    handle NoAnswer => NONE
