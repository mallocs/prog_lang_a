fun same_string(s1 : string, s2 : string) =
    s1 = s2
	     

fun all_except_option(str : string, str_list : string list) =
    case str_list of
	[] => NONE
      | hd::str_list' => case all_except_option(str, str_list') of
			     NONE => if same_string(str, hd)
				     then SOME str_list'
				     else NONE
			   | SOME xs => SOME (hd::xs)

					     
fun get_substitutions1(str_list_list : string list list, s : string) =
    case str_list_list of
	[] => []
      | hd_list::str_list_list' => (case all_except_option(s, hd_list) of
					NONE => [] @ get_substitutions1(str_list_list', s)
				      | SOME xs => xs @ get_substitutions1(str_list_list', s))

				       
fun get_substitutions2(str_list_list : string list list, s : string) =
    let fun f (xs, acc) =
	    case xs of
		[] => acc
	      | hd_list::xs' => (case all_except_option(s, hd_list) of
				     NONE => f(xs', acc @ [])
				   | SOME xs'' => f(xs', acc @ xs''))
    in
	f(str_list_list, [])
    end

	
fun similar_names(str_list_list : string list list, {first : string, middle: string, last : string}) =
    let val name = {first = first, middle = middle, last = last }
	fun sub_first_name sub =
	    {first = sub, middle = middle, last = last}
    in
	case str_list_list of
	    [] => [{first = first, middle = middle, last = last}]
	  | hd_list::str_list_list' => (case all_except_option(first, hd_list) of
					    NONE => similar_names(str_list_list', name) @ []
					  | SOME xs => similar_names(str_list_list', name) @ map sub_first_name xs)
    end


(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw 

exception IllegalMove

fun card_color(suit, rank) =
    case suit of
	Clubs => Black : color
      | Spades => Black : color
      | Diamonds => Red : color
      | Hearts => Red : color

			    
fun card_value(suit, rank) =
    case rank of
	Num i => i
      | Ace => 11
      | Jack => 10
      | Queen => 10
      | King => 10

		    
fun remove_card(cs : card list, c : card, e : exn) =
   let fun except(c : card, card_list : card list) =
	   case card_list of
	       [] => []
	     | hd::card_list' => if hd = c
				 then card_list'
				 else hd::except(c, card_list')
   in
       let val saved_cs = except(c, cs) in
	   if saved_cs = cs
	   then raise e
	   else saved_cs
       end
   end

       
fun all_same_color(cs : card list) =
    case cs of
	[] => true
      | _::[] => true
      | head::(neck::rest) => (card_color(head) = card_color(neck) andalso all_same_color (neck::rest))

				  
fun sum_cards(cs : card list) =
    let fun f(cs, acc) =
	    case cs of
		[] => acc
	      | hd::cs' => f(cs', card_value(hd) + acc)
    in
	f(cs, 0)
    end


fun sum_cards_challenge(cs : card list, goal : int) =
    let
	val sum = sum_cards(cs)	   
	fun count_aces(cs : card list) =
	    case cs of
		[] => 0
	      | (_,rank)::cs' => if rank = Ace
				 then 1 + count_aces(cs')
				 else count_aces(cs')
	val diff = sum - goal
    in
	if diff > 0
	then let val tens_to_subtract = (diff div 10) + 1
		 val aces_count = count_aces(cs)
	     in		     
		 if tens_to_subtract > aces_count
		 then sum - aces_count * 10
		 else sum - tens_to_subtract * 10
	     end
	else sum
    end
	
	
fun score(cs : card list, goal : int) =
    let
	val sum = sum_cards(cs)
	fun prelim (sum, goal) =
	    if sum > goal
	    then 3 * (sum - goal)
	    else goal - sum
    in
	if all_same_color(cs)
	then prelim(sum, goal) div 2
	else prelim(sum, goal)
						  
    end

			    
(* in global scope so it can be shared with challenge problem *)
fun play(cs : card list, mvs : move list, held_cards : card list, goal : int, variable_aces : bool) =
    let fun sum(cs : card list) =
	if variable_aces
	then sum_cards_challenge(cs, goal)
	else sum_cards(cs)
    in
	case mvs of
	    [] => held_cards
	  | (Discard c)::mvs' => play(cs, mvs', remove_card(held_cards, c, IllegalMove), goal, variable_aces)
	  | (Draw)::mvs' => (case cs of
				 [] => held_cards
			       | hd::cs' => if sum(hd::held_cards) > goal
					    then hd::held_cards
					    else play(cs', mvs', hd::held_cards, goal, variable_aces))
    end

				    
fun officiate(cs : card list, mvs : move list, goal : int) =
    score(play(cs, mvs, [], goal, false), goal)


fun score_challenge(cs: card list, goal : int) =
    let
	 (* ugly, but minimum score can happen when we're over by up to 3 points *) 
 	val sum = sum_cards_challenge(cs, goal + 4)  
	fun prelim(sum, goal) =
	    if sum > goal
	    then 3 * (sum - goal)
	    else goal - sum
    in
	if all_same_color(cs)
	then prelim(sum, goal) div 2
	else prelim(sum, goal)				  
    end

	
fun officiate_challenge(cs : card list, mvs : move list, goal : int) =
    score_challenge(play(cs, mvs, [], goal, true), goal)
